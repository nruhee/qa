package neemble;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;


public class Finance {
    WebDriver driver;
    String authorizationEmail;
    String authorizationPassword;


    @Before
    public void openBrowser() {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://erp.qa.neemble.ru/");

        authorizationEmail="duxy14@list.ru";
        authorizationPassword="baranova";
    }
    @After
    public void closeBrowser() throws IOException {
        // driver.quit();
    }

    public  void stupidWait(int timeout){

        try {
            Thread.sleep(timeout);
        }
        catch  (InterruptedException ie1) {
            ie1.printStackTrace();
        }
    }
    public  void  loginToApp(){
        // логин
        WebElement login = driver.findElement(By.name("username"));
        WebElement password = driver.findElement(By.name("password"));
        WebElement submit = driver.findElement(By.xpath("//button"));

        login.sendKeys(authorizationEmail);
        password.sendKeys(authorizationPassword);
        submit.click();
    }

    public  void logoutApp(){
        //логаут
        WebElement logout = driver.findElement(By.xpath("//a[@class='navigation__logoutLink']"));
        logout.click();
        WebElement title= driver.findElement(By.xpath("//h2[@class='page__loginTitle']"));
        assertEquals("Вход в систему", title.getText());
    }

    public void changeContext(int sellPointIndex, String sellPointName){
        //выбор селл поинта
        WebElement contextDropdown=driver.findElement(By
                .xpath("//div[@class='dropdown__head']"));
        contextDropdown.click();
        WebElement contextDropdownSP=driver.findElement(By
                .xpath("//div[@class='dropdown__body']/ul/li["+(1+sellPointIndex)+"]"));
        contextDropdownSP.click();

        stupidWait(3000);

        // проверка
        WebElement contextName=driver.findElement(By
                .xpath("//div[@class='head__title']"));
        assertEquals(sellPointName,contextName.getText());
    }

    public void checkSalesHeader(String countSales, String countCancel, String countBack ){
        //проверка заголовка продаж, отмен и возвратов
        WebElement salesCount=driver.findElement(By
                .xpath("//div[1][@class='page_sales__totalNumber']"));
        assertEquals(countSales,salesCount.getText());

        WebElement cancelCount=driver.findElement(By
                .xpath("//div[@class='page_sales__totalItem page_sales__totalItemActions']/div[2]/div"));
        assertEquals(countCancel,cancelCount.getText());

        WebElement backCount=driver.findElement(By
                .xpath("//div[@class='page_sales__totalItem page_sales__totalItemActions']/div[3]/div"));
        assertEquals(countBack,backCount.getText());
    }

    public  void checkGoodsHeader(String countGoods, String countSum){
        //проверка заголовка проданных товаров
        WebElement goodsCount=driver.findElement(By
                .xpath("//div[@class='page_sales__totalItem page_sales__totalItemSales']/div[1]/div"));
        assertEquals(countGoods,goodsCount.getText());

        WebElement sumCount=driver.findElement(By
                .xpath("//div[@class='page_sales__totalItem page_sales__totalItemSales']/div[2]/div"));
        assertEquals(countSum+" Р",sumCount.getText());

    }

    public  void checkMoneyHeader(String countCash, String countCard){
        //проверка заголовка денег
        WebElement cashCount=driver.findElement(By
                .xpath("//div[@class='page_sales__totalItem page_sales__totalItemMoney page_sales__totalItemMoneyNull']/div[2]/div[1]/div"));
        assertEquals(countCash+" Р",cashCount.getText());

        WebElement cardCount=driver.findElement(By
                .xpath("//div[@class='page_sales__totalItem page_sales__totalItemMoney page_sales__totalItemMoneyNull']/div[2]/div[2]/div"));
        assertEquals(countCard+" Р",cardCount.getText());

    }

    public  void checkDefaultPeriod(){
        //проверка дефолтной даты
        WebElement dateTo=driver.findElement(By.name("dateTo"));
        WebElement dateFrom=driver.findElement(By.name("dateFrom"));

        Locale local = new Locale("ru","RU");
        DateFormat df = DateFormat.getDateInstance(DateFormat.DEFAULT, local);

        Calendar cal = Calendar.getInstance();

        cal.add(Calendar.DATE, 0);
        String formattedDatTo=df.format(cal.getTime());

        cal.add(Calendar.DATE,-7);
        String formattedDateFrom=df.format(cal.getTime());

        assertEquals(formattedDatTo, dateTo.getAttribute("value"));
        assertEquals(formattedDateFrom,dateFrom.getAttribute("value"));
    }

    public void checkEmptyTable(){
        //проверка пустой таблицы продаж
        WebElement alert=driver.findElement(By.xpath("//div[@class='tableWrap_sales']/div"));
        assertEquals("Нет ни одной продажи.", alert.getText());

    }

   @Test
   public void runTest () {

      loginToApp();

       //переход на финансы
       WebElement finLink = driver.findElement(By.xpath("//div[text()='Финансы']"));
       finLink.click();

        //проверка заголовка страницы
       WebElement contextName=driver.findElement(By
               .xpath("//div[@class='content__title']"));
       assertEquals("Продажи",contextName.getText());

       //проверка даты на календаре
       checkDefaultPeriod();

       //проверка значений в заголовках
       checkSalesHeader("0","0","0");
       checkGoodsHeader("0","0,00");
       checkMoneyHeader("0,00","0,00");

       //проверка пустой таблицы
       checkEmptyTable();

       //TODO: добавить фильтрацию продаж когда будут данные
       //проверка набора контролов для корневого контекста
       int buttonsCount=driver.findElements(By.xpath("//div[@class='panel-heading content__actions page_sales__actions']/div[@class='content__action']")).size();
       assertEquals(5,buttonsCount);

       //смена контекста контрол для выбора ТТ пропадает
       changeContext(1,"ТТ1");

       buttonsCount=driver.findElements(By.xpath("//div[@class='panel-heading content__actions page_sales__actions']/div[@class='content__action']")).size();
       assertEquals(4,buttonsCount);

       //смена контекста контрол для выбора ТТ пропадает
       changeContext(2,"ТТ2");

       buttonsCount=driver.findElements(By.xpath("//div[@class='panel-heading content__actions page_sales__actions']/div[@class='content__action']")).size();
       assertEquals(4,buttonsCount);

       //логаут
       logoutApp();



   }



}
