package neemble;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;


public class Login {

    WebDriver driver;

    @Test
    public void runTest (){
        WebElement login = driver.findElement(By.name("username"));
        WebElement password = driver.findElement(By.name("password"));
        WebElement submit = driver.findElement(By.xpath("//button"));

        login.sendKeys("n.ruhee@neemble.ru");
        password.sendKeys("plismocok123");
        submit.click();

        WebElement headerName = (new WebDriverWait(driver,10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='head__user']")));
        WebElement headerCompany = (new WebDriverWait(driver,10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='head__title']")));

        // System.out.println(header.getText());
        assertEquals("Ruhee N.",headerName.getText());
        assertEquals("Hudson Bar",headerCompany.getText());

        WebElement logout = driver.findElement(By.xpath("//a[@class='navigation__logoutLink']"));
        logout.click();

        WebElement title= driver.findElement(By.xpath("//h2[@class='page__loginTitle']"));
        assertEquals("Вход в систему", title.getText());

        // System.out.println(title.getText());

    }

    @Before
    public void openBrowser() {
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe" );
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://erp.qa.neemble.ru/");

    }

    @After
    public void closeBrowser() throws IOException {
        driver.quit();
    }
}
