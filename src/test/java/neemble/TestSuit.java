package neemble;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


public class TestSuit {
    WebDriver driver;
    String authorizationEmail;
    String authorizationPassword;

    @Before
    public void openBrowser() {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://erp.qa.neemble.ru/");
        driver.manage().window().maximize();

        authorizationEmail="duxy14@list.ru";
        authorizationPassword="baranova";
    }

    @After
    public void closeBrowser() throws IOException {
        driver.quit();
    }


    @Test
    public void suit() {
        Login loginTest = new Login();
        loginTest.driver = driver;

        Registration registrationTest = new Registration();
        registrationTest.driver=driver;
        registrationTest.authorizationEmail=authorizationEmail;
        registrationTest.authorizationPassword=authorizationPassword;


        Devices devicesTest = new Devices();
        devicesTest.driver = driver;
        devicesTest.authorizationEmail=authorizationEmail;
        devicesTest.authorizationPassword=authorizationPassword;

        Employees employeesTest=new Employees();
        employeesTest.driver=driver;
        employeesTest.authorizationEmail=authorizationEmail;
        employeesTest.authorizationPassword=authorizationPassword;

        Finance financeTest=new Finance();
        financeTest.driver=driver;
        financeTest.authorizationEmail=authorizationEmail;
        financeTest.authorizationPassword=authorizationPassword;

        Menu menuTest=new Menu();
        menuTest.driver=driver;
        menuTest.authorizationEmail=authorizationEmail;
        menuTest.authorizationPassword=authorizationPassword;

        Review reviewTest=new Review();
        reviewTest.driver=driver;
        reviewTest.authorizationEmail=authorizationEmail;
        reviewTest.authorizationPassword=authorizationPassword;

       loginTest.runTest();

        registrationTest.runTest();

        devicesTest.runTest();

        employeesTest.runTest();

        financeTest.runTest();

        reviewTest.runTest();

        menuTest.runTest();





    }
}
