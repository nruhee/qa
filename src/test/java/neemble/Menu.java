package neemble;

import com.sun.xml.internal.bind.v2.TODO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;


public class Menu {

    WebDriver driver;
    String authorizationEmail;
    String authorizationPassword;

    @Before
    public void openBrowser() {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://erp.qa.neemble.ru/");

        authorizationEmail="duxy14@list.ru";
        authorizationPassword="baranova";
    }

    @After
    public void closeBrowser() throws IOException {
          //  driver.quit();
    }

    public  void stupidWait(int timeout){

        try {
            Thread.sleep(timeout);
        }
        catch  (InterruptedException ie1) {
            ie1.printStackTrace();
        }
    }

    public  void  loginToApp(){
        // логин
        WebElement login = driver.findElement(By.name("username"));
        WebElement password = driver.findElement(By.name("password"));
        WebElement submit = driver.findElement(By.xpath("//button"));

        login.sendKeys(authorizationEmail);
        password.sendKeys(authorizationPassword);
        submit.click();
    }

    public  void logoutApp(){
        //логаут
        WebElement logout = driver.findElement(By.xpath("//a[@class='navigation__logoutLink']"));
        logout.click();
        WebElement title= driver.findElement(By.xpath("//h2[@class='page__loginTitle']"));
        assertEquals("Вход в систему", title.getText());
    }

    public void addModifier(int modifierIndex, String value, String price){
        //добавляет модификатор в товар
        WebElement newValue = driver.findElement(By.xpath("//div[@class='inputList inputList_productVariations']/div["+modifierIndex+"]/div/div/input"));
        newValue.clear();
        newValue.sendKeys(value);

        WebElement newPrice=driver.findElement(By.xpath("//div[@class='inputList inputList_productVariations']/div["+modifierIndex+"]/div/div[2]/input"));
        newPrice.clear();
        newPrice.sendKeys(price);

    }

    public  void addNewGood(String gName, String basicValue1, String basicPrice1,String basicValue2, String basicPrice2){
        //добавление товара
        WebElement addGoodButton=driver.findElement(By.xpath("//a[@data-product-id='new']"));
        addGoodButton.click();

        stupidWait(1000);

        //проверка заголовка
        WebElement newGoodTitle=driver.findElement(By.xpath("//div[@class='form__titleText']"));
        assertEquals("Новый товар",newGoodTitle.getText());

        WebElement newGoodName=driver.findElement(By.xpath("//input[@placeholder='Название']"));
        newGoodName.sendKeys(gName);

       addModifier(1,basicValue1,basicPrice1);
       addModifier(2,basicValue2,basicPrice2);

        WebElement saveButton = driver.findElement(By.xpath("//span[text()='Сохранить']"));
        saveButton.click();

    }
    public  void checkGood(String gName, String gPrice){

        //проверка что товар сохранился
        WebElement goodTitle=driver.findElement(By.xpath("//div[@class='dataGrid__itemOuter menuGrid__product']/div/div/div"));
        assertEquals(gName,goodTitle.getText());

        WebElement goodFooter=driver.findElement(By.xpath("//div[@class='dataGrid__itemOuter menuGrid__product']/div/div/div[2]"));
        assertEquals("от "+gPrice+" \u20BD",goodFooter.getText());

    }

    public  void addNewCategory(String cName, int colorIndex){
        //добавление категории
        WebElement addCategoryButton=driver.findElement(By.xpath("//a[@data-category-id='new']"));
        addCategoryButton.click();

        stupidWait(1000);
         //проверка заголовка
        WebElement newGoodTitle=driver.findElement(By.xpath("//div[@class='form__titleText']"));
        assertEquals("Новая категория",newGoodTitle.getText());

        //имя
        WebElement newCategoryName=driver.findElement(By.name("category.name"));
        newCategoryName.sendKeys(cName);

        //выбор цвета
        WebElement colorButton=driver.findElement(By.xpath("//div[@class='form__color']"));
        colorButton.click();

        stupidWait(1000);

        WebElement colorIcon=driver.findElement(By.xpath("//div[@class='form__colors']/label["+colorIndex+"]"));
        colorIcon.click();

        WebElement saveColorButton=driver.findElement(By.xpath("//span[text()='Готово']"));
        saveColorButton.click();

        //сохранение категории
        stupidWait(1000);
        WebElement saveCategoryButton = driver.findElement(By.xpath("//span[text()='Сохранить']"));
        saveCategoryButton.click();

    }

    public  void checkCategory(String cName, int numberOfGoods){
        //проверка что категория сохранилась
        WebElement categoryTitle=driver.findElement(By.xpath("//div[@class='dataGrid__itemOuter menuGrid__folder']/div/div/div/div"));
        assertEquals(cName,categoryTitle.getText());

        WebElement categoryFooter=driver.findElement(By.xpath("//div[@class='dataGrid__itemOuter menuGrid__folder']/div/div/div[2]"));
        assertEquals(numberOfGoods+" товаров",categoryFooter.getText());

    }

    public void  createNewMenu(int sellPointIndex){

        //переход на редактирование селл поинта, чтобы создать новое меню
        //открытие меню
        WebElement contextDropdown=driver.findElement(By
                .xpath("//div[@class='dropdown__head']"));
        contextDropdown.click();
        WebElement contextDropdownSP=driver.findElement(By
                .xpath("//div[@class='dropdown__body']/ul/li["+(1+sellPointIndex)+"]"));
        //наведение мыши на пункт меню, чтобы появилась кнопка редактирования торговой точки
        Actions builder = new Actions(driver);
        builder.moveToElement(contextDropdownSP).perform();

        //открытые формы редактирования селл поинта
        WebElement editSP = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By
                        .xpath("//div[@class='dropdown__body']/ul/li["+(1+sellPointIndex)+"]/span[@class='dropdown_context__sellPointEdit']")));
        editSP.click();

        stupidWait(1000);

        // раскрываем дропдаун
        WebElement menuBut=driver.findElement(By.xpath("//div[3][@class='form__section']/div/div/div"));
        menuBut.click();

        //выбираем пункт создать новое меню
        WebElement menuDD=driver.findElement(By.xpath("//div[@class='dropdown-menu open']/ul/li[2]"));
        menuDD.click();

        //сохраняем
        WebElement saveButton=driver.findElement(By.xpath("//span[@class='button__text']"));
        saveButton.click();
    }

    public void changeContext(int sellPointIndex, String sellPointName){
        //выбор селл поинта
        WebElement contextDropdown=driver.findElement(By
                .xpath("//div[@class='dropdown__head']"));
        contextDropdown.click();
        stupidWait(1000);
        WebElement contextDropdownSP=driver.findElement(By
                .xpath("//div[@class='dropdown__body']/ul/li["+(1+sellPointIndex)+"]"));
        contextDropdownSP.click();

        stupidWait(1000);

        // проверка
        WebElement contextName=driver.findElement(By
                .xpath("//div[@class='head__title']"));
        assertEquals(sellPointName,contextName.getText());
    }

    public void checkEmptyMenu(){
        //
        WebElement emptyMessage=driver.findElement(By.xpath("//div[@class='alert alert-info']"));
        assertEquals("Товаров не найдено.",emptyMessage.getText());
    }

    public void selectMenu(int menuIndex){

        WebElement menuSelect=driver.findElement(By.xpath("//div[@class='btn-group bootstrap-select form-control']"));
        menuSelect.click();

        WebElement menuRow=driver.findElement(By.xpath("//div[@class='dropdown-menu open']/ul/li["+menuIndex+"]"));
        menuRow.click();
    }

    @Test
    public void runTest () {

       loginToApp();

        //переход на меню
        WebElement goodsLink = driver.findElement(By.xpath("//div[text()='Товары']"));
        goodsLink.click();

        //проверка заголовка
        WebElement contextName=driver.findElement(By.xpath("//div[@class='content__title']"));
        assertEquals("Товары",contextName.getText());

        //добавление товара
        addNewGood("Шоколад","Мишка на севере","60","Аленушка","65");
        checkGood("Шоколад","60,00");

        //добавление категории
        addNewCategory("Напитки", 1);
        //задизейблено из-за бага - 0 не отображается в количестве товаров
        //checkCategory("Напитки",0);

        //добавление товара в категорию
        WebElement categoryIcon=driver.findElement(By.xpath("//div[@class='dataGrid__itemOuter menuGrid__folder']"));
        categoryIcon.click();

        stupidWait(1000);

        //проверка что зашли в нужную категорию
        WebElement catTitle=driver.findElement(By.xpath("//div[@class='breadcrumbs']/a[2]"));
        assertEquals("Напитки",catTitle.getText());

        //добавляем товар в категорию
        addNewGood("Лимонад","Буратино","150", "Пепси","200");
        checkGood("Лимонад","150,00");

        //возврат в корень
        WebElement toMainMenuLin=driver.findElement(By.xpath("//div[@class='breadcrumbs']/a[1]"));
        toMainMenuLin.click();

        //проверка изменения количества товаров на категории
        checkCategory("Напитки",1);

        //TODO: добавить проверки на редактирование айтемов меню


        //проверка создания нового меню и его фильтрации по ТТ


        //TODO: добавить проверку что одно меню на обе ТТ

        //создание Меню 2

        //создание нового меню для ТТ2
        createNewMenu(2);
        stupidWait(1000);

        //переход в контекст ТТ2
        changeContext(2,"ТТ2");
        checkEmptyMenu();

        //добавление категории
        addNewCategory("Закуски", 2);
        //задизейблено из-за бага - 0 не отображается в количестве товаров
        //checkCategory("Напитки",0);

        stupidWait(1000);

        //добавление товарав корень
        addNewGood("Сыр","Камамбер","300","ДорБлю","400");
        checkGood("Сыр","300,00");

        //добавление товара в категорию
        categoryIcon=driver.findElement(By.xpath("//div[@class='dataGrid__itemOuter menuGrid__folder']"));
        categoryIcon.click();

        stupidWait(1000);

        //проверка что зашли в нужную категорию
        catTitle=driver.findElement(By.xpath("//div[@class='breadcrumbs']/a[2]"));
        assertEquals("Закуски",catTitle.getText());

        //добавляем товар в категорию
        addNewGood("Салат","Столичный","250", "Винегрет","390");
        checkGood("Салат","250,00");

        //возврат в корень
        toMainMenuLin=driver.findElement(By.xpath("//div[@class='breadcrumbs']/a[1]"));
        toMainMenuLin.click();

        //проверка изменения количества товаров на категории
        checkCategory("Закуски",1);

        //переход в корневой контекст

        changeContext(0,"Мой бизнес");

        //выбор меню 2
        selectMenu(2);
        stupidWait(1000);
        checkGood("Сыр","300,00");
        checkCategory("Закуски",1);

        //выбор меню 1
        selectMenu(1);
        stupidWait(1000);
        checkCategory("Напитки",1);
        checkGood("Шоколад","60,00");

        //логаут
        logoutApp();

    }

}
