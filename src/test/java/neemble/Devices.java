package neemble;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;


//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Devices {

    WebDriver driver;
    String authorizationEmail;
    String authorizationPassword;

    public void addNewDevice(String name, String typeName, String sellPointName){
        //добавление устройства
        WebElement addDevice=driver.findElement(By.xpath("//a[@data-device-id='new']"));
        addDevice.click();
        WebElement deviceName=driver.findElement(By.name("name"));
        deviceName.sendKeys(name);

       // WebElement but1 = (new WebDriverWait(driver, 10))
               // .until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@title='Выберите тип устройства']")));
       stupidWait(1000);
        WebElement but1=driver.findElement(By.xpath("//button[@title='Выберите тип устройства']"));
        but1.click();
       stupidWait(1000);
        WebElement devType=driver.findElement(By.xpath("//span[text()='"+"\n"+typeName+"\n"+"']"));
        devType.click();

        WebElement but2=driver.findElement(By.xpath("//button[@title='Выберите торговую точку']"));
        but2.click();
        stupidWait(1000);
        WebElement sellPoint=driver.findElement(By.xpath("//span[text()='"+sellPointName+"']"));
        sellPoint.click();
        //Select deviceSellPoint=new Select (driver.findElement(By.name("sellPointId")));
        //deviceSellPoint.selectByIndex(sellPointId);

        WebElement saveDevice=driver.findElement(By.xpath("//button"));
        saveDevice.submit();

    }

    public  void checkHeaderInUnregisteredDevicesTable(){
        //проверка заголовков таблицы
        WebElement panel=driver.findElement(By
                .xpath("//div[@class='panel panel_devicesNotRegisteredPanel']/div[@class='panel-heading']"));
        assertEquals("Незарегистрированные устройства",panel.getText());
        WebElement idColumn=driver.findElement(By
                .xpath("//table[@class='table table-hover table-striped table_devicesNotRegistered']/thead/tr/th[@data-sort-by='id']"));
        assertEquals("ID", idColumn.getText());
        WebElement nameColumn=driver.findElement(By
                .xpath("//table[@class='table table-hover table-striped table_devicesNotRegistered']/thead/tr/th[@data-sort-by='name']"));
        assertEquals("Название", nameColumn.getText());
        WebElement spColumn=driver.findElement(By
                .xpath("//table[@class='table table-hover table-striped table_devicesNotRegistered']/thead/tr/th[@data-sort-by='sellPointId']"));
        assertEquals("Торговая точка", spColumn.getText());
        WebElement regColumn=driver.findElement(By
                .xpath("//table[@class='table table-hover table-striped table_devicesNotRegistered']/thead/tr/th[@data-sort-by='registrationCode']"));
        assertEquals("Код регистрации", regColumn.getText());

    }

        public  void checkRowInUnregisteredDevicesTable(String name, String sellPoint, int index){

            //проверка значений в таблице
            WebElement idValue=driver.findElement(By
                    .xpath("//table[@class='table table-hover table-striped table_devicesNotRegistered']/tbody/tr["+index+"]/td[2]"));
            assertNotNull(idValue.getText());
            WebElement nameValue=driver.findElement(By
                    .xpath("//table[@class='table table-hover table-striped table_devicesNotRegistered']/tbody/tr["+index+"]/td[3]"));
            assertEquals(name, nameValue.getText());
            WebElement spValue=driver.findElement(By
                    .xpath("//table[@class='table table-hover table-striped table_devicesNotRegistered']/tbody/tr["+index+"]/td[4]"));
            assertEquals (sellPoint,spValue.getText());
            WebElement codeValue=driver.findElement(By
                    .xpath("//table[@class='table table-hover table-striped table_devicesNotRegistered']/tbody/tr["+index+"]/td[5]"));
            assertNotNull(codeValue.getText());
        }

    public  void checkRowInUnregisteredDevicesTableForSellPoint(String name, int index){

        //проверка значений в таблице
        WebElement idValue=driver.findElement(By
                .xpath("//table[@class='table table-hover table-striped table_devicesNotRegistered']/tbody/tr["+index+"]/td[2]"));

                assertNotNull(idValue.getText());
        WebElement nameValue=driver.findElement(By
                .xpath("//table[@class='table table-hover table-striped table_devicesNotRegistered']/tbody/tr["+index+"]/td[3]"));
        assertEquals(name, nameValue.getText());

        WebElement codeValue=driver.findElement(By
                .xpath("//table[@class='table table-hover table-striped table_devicesNotRegistered']/tbody/tr["+index+"]/td[4]"));
        assertNotNull(codeValue.getText());
    }


    public void changeContext(int sellPointIndex, String sellPointName){
        //выбор селл поинта
        WebElement contextDropdown=driver.findElement(By
                .xpath("//div[@class='dropdown__head']"));
        contextDropdown.click();
        WebElement contextDropdownSP=driver.findElement(By
                .xpath("//div[@class='dropdown__body']/ul/li["+(1+sellPointIndex)+"]"));
        contextDropdownSP.click();

       stupidWait(2000);

        // проверка
        WebElement contextName=driver.findElement(By
                .xpath("//div[@class='head__title']"));
        assertEquals(sellPointName,contextName.getText());
    }

    public  void deleteDevice(){
        WebElement deleteButton = driver.findElement(By.xpath("//span[@class='table_devicesNotRegistered__remove']"));
        deleteButton.click();
    }

    public  void  loginToApp(){
        // логин
        WebElement login = driver.findElement(By.name("username"));
        WebElement password = driver.findElement(By.name("password"));
        WebElement submit = driver.findElement(By.xpath("//button"));

        login.sendKeys(authorizationEmail);
        password.sendKeys(authorizationPassword);
        submit.click();
    }

    public  void logoutApp(){
        //логаут
        WebElement logout = driver.findElement(By.xpath("//a[@class='navigation__logoutLink']"));
        logout.click();
        WebElement title= driver.findElement(By.xpath("//h2[@class='page__loginTitle']"));
        assertEquals("Вход в систему", title.getText());
    }

    public  void stupidWait(int timeout){

        try {
            Thread.sleep(timeout);
        }
        catch  (InterruptedException ie1) {
            ie1.printStackTrace();
        }
    }

    @Before
    public void openBrowser() {
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe" );
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://erp.qa.neemble.ru/");

        authorizationEmail="duxy14@list.ru";
        authorizationPassword="baranova";

    }

    @After
    public void closeBrowser() throws IOException {
       // driver.quit();
    }

    @Test
    public void runTest(){
        loginToApp();

        //переход на "Устройства"

        WebElement link=driver.findElement(By.xpath("//a[@href='/devices']"));
        link.click();
        //driver.manage().timeouts().pageLoadTimeout(5,TimeUnit.SECONDS);

       stupidWait(2000);

        // проверка заголовка и того что устройств нет
        WebElement title=driver.findElement(By.xpath("//div[@class='content__title']"));
        assertEquals("Устройства",title.getText());

        title=driver.findElement(By.xpath("//div[@class='alert alert-info page_devices__alert']"));
        assertEquals("Нет ни одного устройства.",title.getText());

        //добавление кассы
        addNewDevice("Касса1","Касса", "ТТ1");

       stupidWait(2000);

        //проверка что устройство добавилось
        checkHeaderInUnregisteredDevicesTable();

        //проверка значений в таблице
        checkRowInUnregisteredDevicesTable("Касса1","ТТ1",1);

        //добавление микрокомпьютера
        addNewDevice("МК1","Микрокомпьютер","ТТ1");

        stupidWait(2000);


        //добавление второй кассы во второй селл поинт
        addNewDevice("Касса2", "Касса", "ТТ2");

        stupidWait(2000);

        //добавление микрокомпьютера
        addNewDevice("МК2","Микрокомпьютер","ТТ2");

        stupidWait(2000);

        //костыль - чтобы увидеть новую кассу в списке
       // driver.navigate().refresh();

        //проверка значений в таблице - задизейблено так как устройства каждый раз приходят в разном порядке
//        checkRowInUnregisteredDevicesTable("Касса2","ТТ2",2);

        //проверка фильтрации списка устройств по селл поинту
        changeContext(1,"ТТ1");

        //проверка значений в таблице
        checkRowInUnregisteredDevicesTableForSellPoint("Касса1",1);
        checkRowInUnregisteredDevicesTableForSellPoint("МК1",2);


        //проверка фильтрации списка устройств по селл поинту
        changeContext(2,"ТТ2");

        //проверка значений в таблице
        checkRowInUnregisteredDevicesTableForSellPoint("Касса2",1);
        checkRowInUnregisteredDevicesTableForSellPoint("МК2",2);

        //переход в корневой контекст
        changeContext(0,"Мой бизнес");

        //проверка всех устройств в таблице
        checkRowInUnregisteredDevicesTableForSellPoint("Касса1",1);
        checkRowInUnregisteredDevicesTableForSellPoint("МК1",3);
        checkRowInUnregisteredDevicesTableForSellPoint("Касса2",2);
        checkRowInUnregisteredDevicesTableForSellPoint("МК2",4);


        //удаление устройства1
        deleteDevice();

        stupidWait(2000);

        checkRowInUnregisteredDevicesTableForSellPoint("МК1",2);
        checkRowInUnregisteredDevicesTableForSellPoint("Касса2",1);
        checkRowInUnregisteredDevicesTableForSellPoint("МК2",3);

        //удаление устройства 2
        deleteDevice();

        stupidWait(2000);

        checkRowInUnregisteredDevicesTableForSellPoint("МК1",1);
        checkRowInUnregisteredDevicesTableForSellPoint("МК2",2);

        //удаление устройства 3
        deleteDevice();

        stupidWait(2000);

        checkRowInUnregisteredDevicesTableForSellPoint("МК2",1);

        //удаление уйстройства 4
        deleteDevice();

        stupidWait(2000);

        //рефреш страницы
        driver.navigate().refresh();

        //проверка что устройств нет
        title=driver.findElement(By.xpath("//div[@class='alert alert-info page_devices__alert']"));
        assertEquals("Нет ни одного устройства.",title.getText());

        //логаут
       logoutApp();


    }

}
