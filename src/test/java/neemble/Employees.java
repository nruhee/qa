package neemble;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import static junit.framework.Assert.assertEquals;


public class Employees {

    WebDriver driver;
    String authorizationEmail;
    String authorizationPassword;

    public  void addPosition(String posName, String posDescription){


        //создание должности

        WebElement addPosition=driver.findElement(By.xpath("//a[@class='button button_green']"));
        addPosition.click();

        WebElement positionName=driver.findElement(By.name("name"));
        WebElement positionDesc=driver.findElement(By.name("description"));
        WebElement savePosition=driver.findElement(By.xpath("//button"));

        positionName.sendKeys(posName);
        positionDesc.sendKeys(posDescription);
        savePosition.submit();



    }

    public  void addEmployee(String posValue, String spValue, String lName, String fName, String patr,
    String phoneNumber, String emailValue, String pinValue ,String passwordValue    ){

        //создание сотрудника
        WebElement addEmployee=driver.findElement(By.xpath("//a[@class='button button_green page_employees__buttonAdd']"));
        addEmployee.click();

        WebElement  lastName=driver.findElement(By.name("lastName"));
        WebElement  firstName=driver.findElement(By.name("firstName"));
        WebElement  patronymic=driver.findElement(By.name("patronymic"));
        //Select position=new Select (driver.findElement(By.name("positionId")));
        //Select sellPoint=new Select (driver.findElement(By.name("sellPointId")));

        WebElement but1=driver.findElement(By.xpath("//button[@title='Выберите должность']"));
        Actions actions = new Actions(driver);
        actions.moveToElement(but1).perform();
        stupidWait(1000);
        but1.click();

        WebElement position=driver.findElement(By.xpath("//span[text()='"+posValue+"']"));
        position.click();

        WebElement but2=driver.findElement(By.xpath("//button[@title='Выберите торговую точку']"));
        Actions actions1 = new Actions(driver);
        actions.moveToElement(but2).perform();
        stupidWait(1000);
        but2.click();
        WebElement sellPoint=driver.findElement(By.xpath("//span[text()='"+spValue+"']"));
        sellPoint.click();

        WebElement phone=driver.findElement(By.name("phoneNumber"));
        WebElement email=driver.findElement(By.name("email"));
        WebElement pin=driver.findElement(By.name("pin"));
        WebElement passw=driver.findElement(By.name("password"));
        WebElement saveEmployee=driver.findElement(By.xpath("//button"));

        lastName.sendKeys(lName);
        firstName.sendKeys(fName);
        patronymic.sendKeys(patr);
       // position.selectByVisibleText(posValue);
      //  sellPoint.selectByVisibleText(spValue);
        phone.sendKeys(phoneNumber);
        email.sendKeys(emailValue);
        pin.sendKeys(pinValue);
        passw.sendKeys(passwordValue);
        saveEmployee.submit();

    }

    public void checkPosition(int positionIndex, String posName, String posDesc){
        // название
        WebElement positionName=driver.findElement(By.xpath("//table[@class='table table-hover table-striped']/tbody/tr["+positionIndex+"]/td[1]"));
        assertEquals(posName,positionName.getText());
        //описание
        WebElement positionDesc=driver.findElement(By.xpath("//table[@class='table table-hover table-striped']/tbody/tr["+positionIndex+"]/td[2]"));
        assertEquals(posDesc,positionDesc.getText());
    }

    public void checkEmployee(int empIndex, String ePos, String eSellPoint, String eName,String eEmail){
        // должность
        WebElement empPos=driver.findElement(By.xpath("//table[@class='table table-hover table-striped']/tbody/tr["+empIndex+"]/td[2]"));
        assertEquals(ePos,empPos.getText());

        // торговая точка
        WebElement empSellPoint=driver.findElement(By.xpath("//table[@class='table table-hover table-striped']/tbody/tr["+empIndex+"]/td[4]"));
        assertEquals(eSellPoint,empSellPoint.getText());

        //имя
        WebElement empName=driver.findElement(By.xpath("//table[@class='table table-hover table-striped']/tbody/tr["+empIndex+"]/td[1]"));
        assertEquals(eName,empName.getText());

        //email
        WebElement empEmail=driver.findElement(By.xpath("//table[@class='table table-hover table-striped']/tbody/tr["+empIndex+"]/td[3]"));
        assertEquals(eEmail,empEmail.getText());

    }

    public  void checkEmployeeForSellPoint(int empIndex, String ePos, String eName, String eEmail){
        // должность
        WebElement empPos=driver.findElement(By.xpath("//table[@class='table table-hover table-striped']/tbody/tr["+empIndex+"]/td[2]"));
        assertEquals(ePos,empPos.getText());

        //имя
        WebElement empName=driver.findElement(By.xpath("//table[@class='table table-hover table-striped']/tbody/tr["+empIndex+"]/td[1]"));
        assertEquals(eName,empName.getText());

        //
        WebElement empEmail=driver.findElement(By.xpath("//table[@class='table table-hover table-striped']/tbody/tr["+empIndex+"]/td[3]"));
        assertEquals(eEmail,empEmail.getText());
    }

    public void changeContext(int sellPointIndex, String sellPointName){
        //выбор селл поинта
        WebElement contextDropdown=driver.findElement(By
                .xpath("//div[@class='dropdown__head']"));
        contextDropdown.click();
        WebElement contextDropdownSP=driver.findElement(By
                .xpath("//div[@class='dropdown__body']/ul/li["+(1+sellPointIndex)+"]"));
        contextDropdownSP.click();

       stupidWait(1000);

        // проверка
        WebElement contextName=driver.findElement(By
                .xpath("//div[@class='head__title']"));
        assertEquals(sellPointName,contextName.getText());
    }

    public void deleteEmployee(int empIndex){

        WebElement row=driver.findElement(By.xpath("//table[@class='table table-hover table-striped']/tbody/tr["+empIndex+"]"));
        row.click();

        WebElement deleteButton=driver.findElement(By.xpath("//a[@class='form__action form__removeLink']"));
        Actions actions = new Actions(driver);
        actions.moveToElement(deleteButton).perform();

        stupidWait(3000);

        deleteButton.click();
    }

    public void checkEmptyEmployeesTable(){
        WebElement emptyAlert=driver.findElement(By.xpath("//div[@class='alert alert-info']"));
        assertEquals("Нет ни одного сотрудника.",emptyAlert.getText());
    }
    public void checkEmptyPositionsTable(){
        WebElement emptyAlert=driver.findElement(By.xpath("//div[@class='alert alert-info']"));
        assertEquals("Нет ни одной должности.",emptyAlert.getText());
    }

    public  void deletePosition(int posIndex){
        //открыть карточку сотрудника
        WebElement row=driver.findElement(By.xpath("//table[@class='table table-hover table-striped']/tbody/tr["+posIndex+"]"));
        row.click();
        stupidWait(2000);
        //удалить
        WebElement deleteButton=driver.findElement(By.xpath("//a[@class='form__action form__removeLink']"));
        deleteButton.click();
    }
    public  void  loginToApp(){
        // логин
        WebElement login = driver.findElement(By.name("username"));
        WebElement password = driver.findElement(By.name("password"));
        WebElement submit = driver.findElement(By.xpath("//button"));

        login.sendKeys(authorizationEmail);
        password.sendKeys(authorizationPassword);
        submit.click();
    }

    public  void logoutApp(){
        //логаут
        WebElement logout = driver.findElement(By.xpath("//a[@class='navigation__logoutLink']"));
        logout.click();
        WebElement title= driver.findElement(By.xpath("//h2[@class='page__loginTitle']"));
        assertEquals("Вход в систему", title.getText());
    }

    public  void stupidWait(int timeout){

        try {
            Thread.sleep(timeout);
        }
        catch  (InterruptedException ie1) {
            ie1.printStackTrace();
        }
    }

    @Before
    public void openBrowser() {
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe" );
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://erp.qa.neemble.ru/");

        authorizationEmail="duxy14@list.ru";
        authorizationPassword="baranova";

    }

    @After
    public void closeBrowser() throws IOException {
       // driver.quit();
    }

    @Test
    public void runTest(){

       loginToApp();

        //переход на "Персонал"
       WebElement link=driver.findElement(By.xpath("//a[@href='/staff/employees']"));
       link.click();

       stupidWait(1000);

       //проверка что создан управляющий
       checkEmployee(1,"Управляющий","","Управляющий Nadia",authorizationEmail);

       //переход на должности
        link=driver.findElement(By.xpath("//a[@href='/staff/positions']"));
        link.click();

        stupidWait(1000);

        //проверка что в списке 3 должности
        checkPosition(1,"Управляющий","");
        checkPosition(2,"Менеджер","");
        checkPosition(3,"Кассир","");


        //создание и проверка должностей
        addPosition("Официант","Приносит клиентам еду");
        checkPosition(4,"Официант","Приносит клиентам еду");
        addPosition("Повар","Готовит еду");
        checkPosition(5,"Повар","Готовит еду");

        //переход на сотрудников
        link=driver.findElement(By.xpath("//a[@href='/staff/employees']"));
        link.click();

        stupidWait(2000);

        //создание и проверка сотрудников
        addEmployee("Официант","ТТ1","Иванов","Иван","Иванович","+79019876543","ivanov1@mail.ru","1917","ivanov123");
        checkEmployee(2,"Официант","ТТ1","Иванов Иван Иванович","ivanov1@mail.ru");

        addEmployee("Повар","ТТ2","Петров","Петр","Петрович","+79329876543","pertov1@mail.ru","1945","petrov123");
        checkEmployee(3,"Повар","ТТ2","Петров Петр Петрович","pertov1@mail.ru");


        //проверка фильтрации по торговым точкам
        changeContext(1,"ТТ1");
        checkEmployeeForSellPoint(1, "Официант", "Иванов Иван Иванович","ivanov1@mail.ru");

        changeContext(2, "ТТ2");
        checkEmployeeForSellPoint(1, "Повар","Петров Петр Петрович","pertov1@mail.ru");

        //переход в корневой контекст
        changeContext(0,"Мой бизнес");

        checkEmployee(2,"Официант","ТТ1","Иванов Иван Иванович","ivanov1@mail.ru");
        checkEmployee(3,"Повар","ТТ2","Петров Петр Петрович","pertov1@mail.ru");

        //удаление сотрудников
        deleteEmployee(3);
        stupidWait(1000);
        deleteEmployee(2);
        stupidWait(1000);

        //проверка что сотрудники удалены, остался только управляющий
        int empCount=driver.findElements(By.xpath("//table[@class='table table-hover table-striped']/tbody/tr")).size();
        assertEquals(1,empCount);

        //переход на должности
       link=driver.findElement(By.xpath("//a[@href='/staff/positions']"));
       link.click();

        //удаление должностей
        deletePosition(5);
        stupidWait(1000);
        deletePosition(4);
        stupidWait(1000);

        //проверка что все должности кроме дефолтных удалены
        int posCount=driver.findElements(By.xpath("//table[@class='table table-hover table-striped']/tbody/tr")).size();
        assertEquals(3,posCount);

      //логаут
       logoutApp();





    }
}
