package neemble;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;


public class Review {
    WebDriver driver;
    String authorizationEmail;
    String authorizationPassword;

    @Before
    public void openBrowser() {
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe" );
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://erp.qa.neemble.ru/");

        authorizationEmail="n.ruhee@neemble.ru";
        authorizationPassword="plismocok123";

    }

    @After
    public void closeBrowser() throws IOException {
       //  driver.close();
    }

    public  void  loginToApp(){
        // логин
        WebElement login = driver.findElement(By.name("username"));
        WebElement password = driver.findElement(By.name("password"));
        WebElement submit = driver.findElement(By.xpath("//button"));

        login.sendKeys(authorizationEmail);
        password.sendKeys(authorizationPassword);
        submit.click();
     }

    public  void logoutApp(){
        //логаут
        WebElement logout = driver.findElement(By.xpath("//a[@class='navigation__logoutLink']"));
        logout.click();
        WebElement title= driver.findElement(By.xpath("//h2[@class='page__loginTitle']"));
        assertEquals("Вход в систему", title.getText());
    }

    public  void stupidWait(int timeout){

        try {
            Thread.sleep(timeout);
        }
        catch  (InterruptedException ie1) {
            ie1.printStackTrace();
        }
    }

    public void checkDateCalendar(String date, int shift, String revenuSum, String profitSum, String averageSum,
                                  String salesCount, String cashPercentage, String cardPercentage ){
        //проверка календаря

        WebElement dateLink=driver.findElement(By.xpath("//a[text()='"+date+"']"));
        dateLink.click();

        stupidWait(1000);

        WebElement calendarValue=driver.findElement(By.xpath("//div[@class='dateRange__value']"));

        Locale local = new Locale("ru","RU");
        DateFormat df = DateFormat.getDateInstance(DateFormat.DEFAULT, local);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        String formattedDate1=df.format(cal.getTime());

        cal.add(Calendar.DATE,shift);
        String formattedDate2=df.format(cal.getTime());
        String formattedDate="";

        if (shift==-7) {
            formattedDate = formattedDate2 + " - " + formattedDate1;
        }
        else{
            formattedDate=formattedDate2;
        }
        //проверка даты в календаре
        assertEquals(formattedDate, calendarValue.getText());
        //проверка данных на иконках виджетов
        checkRevenuWidget(revenuSum);
        checkProfitWidget(profitSum);
        checkAverageCountWidget(averageSum);
        checkPaymentMethodWidget(cashPercentage,cardPercentage);


        //проверка значений в виджете выручки
        checkProfitWidgetPanel(salesCount,profitSum,averageSum,formattedDate);
        checkRatingWidgetPanel(formattedDate);
    }

    public void checkPeriodCalendar(String period, String revenuSum, String profitSum, String averageSum,
                                    String salesCount,String sCash, String sCard){

        WebElement dateLink=driver.findElement(By.xpath("//a[text()='"+period+"']"));
        dateLink.click();

        stupidWait(1000);

        WebElement calendarValue=driver.findElement(By.xpath("//div[@class='dateRange__value']"));

        Locale local = new Locale("ru","RU");
        DateFormat df = DateFormat.getDateInstance(DateFormat.DEFAULT, local);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        String formattedDate1=df.format(cal.getTime());
        if(period=="Месяц"){
            cal.add(Calendar.MONTH,-1);
        }

        else{
            cal.add(Calendar.YEAR,-1);
        }

        String formattedDate2=df.format(cal.getTime());
        String formattedDate=formattedDate2+" - "+formattedDate1;
        //проверка даты в клендаре
        assertEquals(formattedDate, calendarValue.getText());
        //проверка данных на иконке виджетов
        checkRevenuWidget(revenuSum);
        checkProfitWidget(profitSum);
        checkAverageCountWidget(averageSum);
        checkPaymentMethodWidget(sCash,sCard);

        //проверка значений в виджете выручки
        checkProfitWidgetPanel(salesCount,profitSum,averageSum,formattedDate);
        //на виджете Рейтинга
        checkRatingWidgetPanel(formattedDate);


    }

    public  void checkRevenuWidget(String sum){

        WebElement revenuSum=driver.findElement(By.xpath("//div[@class='revenueWidget__title']/strong"));
        assertEquals(sum+" ₽",revenuSum.getText());
    }

    public void checkProfitWidget(String sum){
        WebElement profitSum=driver.findElement(By.xpath("//div[@class='row']/div[1]/div/div/div[2]"));
        assertEquals(sum+" ₽",profitSum.getText());
    }

    public void checkAverageCountWidget(String sum){
        WebElement profitSum=driver.findElement(By.xpath("//div[@class='row']/div[2]/div/div/div[2]"));
        assertEquals(sum+" ₽",profitSum.getText());
    }

    public void checkPaymentMethodWidget(String sCash, String sCard){
        WebElement cashPerc=driver.findElement(By.xpath("//div[@class='reportWidget_paymentType__cashLegend']"));
        assertEquals(sCash+"%",cashPerc.getText());

        WebElement cardPerc=driver.findElement(By.xpath("//div[@class='reportWidget_paymentType__cardLegend']"));
        assertEquals(sCard+"%",cardPerc.getText());

    }

    public void checkProfitWidgetPanel(String salesCount, String profSum, String avValue, String formattedDate){
        //переход на форму виджета выручки
        WebElement profitWidget=driver.findElement(By.xpath("//div[@class='reportWidget__title']"));
        profitWidget.click();

        WebElement profitDate=driver.findElement(By.xpath("//div[@class='dateRange__value']"));
        assertEquals(formattedDate,profitDate.getText());

        WebElement sCount=driver.findElement(By.xpath("//div[@class='modal_revenue__total']/div[1]/div"));
        assertEquals(salesCount,sCount.getText());

        WebElement profitValue=driver.findElement(By.xpath("//div[@class='modal_revenue__total']/div[2]/div"));
        assertEquals(profSum + " Р", profitValue.getText());

        WebElement averageValue=driver.findElement(By.xpath("//div[@class='modal_revenue__total']/div[3]/div"));
        //задизейблено из-за бага с разными значениями на иконке и на карточке
        // assertEquals( avValue+ " Р", averageValue.getText());

        //закрываем виджет
        WebElement closeButton=driver.findElement(By.xpath("//a[@class='modal__closeLink modal__closeLink_bottom']"));
        closeButton.click();
    }

    public  void checkRatingWidgetPanel(String formattedDate){
        //открыть виджет с рейтингом
        WebElement ratingWidget=driver.findElement(By.xpath("//div[@class='reportWidget reportWidget_yellow reportWidget_topProducts']"));
        ratingWidget.click();

        WebElement ratingDate=driver.findElement(By.xpath("//div[@class='dateRange__value']"));
        assertEquals(formattedDate,ratingDate.getText());

        stupidWait(1000);

        //закрываем виджет
        WebElement closeButton=driver.findElement(By.xpath("//a[@class='modal__closeLink modal__closeLink_bottom']"));
        closeButton.click();

    }



    @Test
    public void runTest(){
        //логин в приложение
        loginToApp();

        //проверка всех периодов
        checkDateCalendar("Сегодня",0,"0,00","0,00","0,00","0","0","0");
        checkDateCalendar("Вчера",-1,"0,00","0,00","0,00","0","0","0");
        checkDateCalendar("Неделя",-7,"0,00","0,00","0,00","0","0","0");
       //баг с календарем
       // checkPeriodCalendar("Месяц", "0,00", "0,00", "0,00","0","0","0");
        checkPeriodCalendar("Год","0,00","0,00","0,00","0","0","0");







        //  logoutApp();

    }
}
