package neemble;

import com.sun.xml.internal.bind.v2.TODO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;


public class Registration {

    WebDriver driver;
    String authorizationEmail;
    String authorizationPassword;

    @Before
    public void openBrowser() {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://erp.qa.neemble.ru/");
       driver.manage().window().maximize();

        authorizationEmail="duxy14@list.ru";
        authorizationPassword="baranova";

    }

    @After
    public void closeBrowser() throws IOException {
       // driver.quit();
    }

    public  void stupidWait(int timeout){

        try {
            Thread.sleep(timeout);
        }
        catch  (InterruptedException ie1) {
            ie1.printStackTrace();
        }
    }

    public  void logoutApp() {
        //логаут
        WebElement logout = driver.findElement(By.xpath("//a[@class='navigation__logoutLink']"));
        logout.click();
        WebElement title = driver.findElement(By.xpath("//h2[@class='page__loginTitle']"));
        assertEquals("Вход в систему", title.getText());

    }

    public  void checkSellPointData(int sellPointIndex, String sellPointName, String sellPointAddress,
                                    String sellPointINN, String sellPointMenu){
        //открытие меню
        WebElement contextDropdown=driver.findElement(By
                .xpath("//div[@class='dropdown__head']"));
        contextDropdown.click();
        //проверка названий торговых точек в меню контекста
        WebElement contextDropdownSP=driver.findElement(By
                .xpath("//div[@class='dropdown__body']/ul/li["+(1+sellPointIndex)+"]"));
        assertEquals("•"+sellPointName,contextDropdownSP.getText());
        //наведение мыши на пункт меню, чтобы появилась кнопка редактирования торговой точки
        Actions builder = new Actions(driver);
        builder.moveToElement(contextDropdownSP).perform();
        //открытые формы редактирования селл поинта
       WebElement editSP = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By
                        .xpath("//div[@class='dropdown__body']/ul/li[" + (1 + sellPointIndex) + "]/span[@class='dropdown_context__sellPointEdit']")));

       // WebElement editSP=driver.findElement(By
             //  .xpath("//div[@class='dropdown__body']/ul/li[" + (1 + sellPointIndex) + "]/span[@class='dropdown_context__sellPointEdit']"));
        editSP.click();

       stupidWait(2000);
        //проверка заголовка формы
        WebElement spTitle=driver.findElement(By.xpath("//div[@class='form__titleText']"));
        assertEquals(sellPointName,spTitle.getText());

        //проверка адреса
        WebElement spAddres=driver.findElement(By.name("address"));
        assertEquals(sellPointAddress,spAddres.getText());

        //проверка ИНН
        WebElement spINN=driver.findElement(By.name("inn"));
        assertEquals(sellPointINN,spINN.getAttribute("value"));

        //проверка Меню
        WebElement spMenu=driver.findElement(By.xpath("//span[@class='filter-option pull-left']"));
        assertEquals(sellPointMenu, spMenu.getText());

        //проверка чекбокса
        WebElement spUseTables=driver.findElement(By.name("useTables"));
        assertTrue(spUseTables.isSelected());

        //проверка залов задизейблено так как залы приходят не отсортированные

        if (sellPointIndex==1){
            //проверяем 2 зала
            WebElement room1Name=driver.findElement(By.xpath("//div[@class='inputList inputList_room']/div[@class='inputList__item'][1]/div/div/input"));
           //assertEquals("Большой зал",room1Name.getAttribute("value"));

            WebElement room1Tables=driver.findElement(By.xpath("//div[@class='inputList inputList_room']/div[@class='inputList__item'][1]/div/div[2]/input"));
            assertEquals("10",room1Tables.getAttribute("value"));

            WebElement room2Name=driver.findElement(By.xpath("//div[@class='inputList inputList_room']/div[@class='inputList__item'][2]/div/div/input"));
           // assertEquals("Средний зал",room2Name.getAttribute("value"));

            WebElement room2Tables=driver.findElement(By.xpath("//div[@class='inputList inputList_room']/div[@class='inputList__item'][2]/div/div[2]/input"));
            assertEquals("10",room2Tables.getAttribute("value"));


        }
        else{
            // проверяем 1 зал
            WebElement room1Name=driver.findElement(By.xpath("//div[@class='inputList inputList_room']/div[@class='inputList__item'][1]/div/div/input"));
            assertEquals("Малый зал",room1Name.getAttribute("value"));

            WebElement room1Tables=driver.findElement(By.xpath("//div[@class='inputList inputList_room']/div[@class='inputList__item'][1]/div/div[2]/input"));
            assertEquals("10",room1Tables.getAttribute("value"));

        }

      //закрытие формы
        WebElement closeButton=driver.findElement(By.xpath("//a[@class='modal__closeLink modal__closeLink_bottom']"));
        closeButton.click();
    }

    @Test
    public void runTest() {

        //переход на страницу регистрации
        WebElement registrationLink = driver.findElement(By.xpath("//a[@href='/registration']"));
        registrationLink.click();

        //проверка заголовка новой страницы
        WebElement title = driver.findElement(By.xpath("//h2[@class='page__registrationTitle']"));
        assertEquals("Регистрация", title.getText());

        //создание нового юзера
        WebElement userName = driver.findElement(By.name("firstName"));
        WebElement email = driver.findElement(By.name("email"));
        WebElement password = driver.findElement(By.name("password"));
        WebElement submit = driver.findElement(By.xpath("//button"));

        userName.sendKeys("superman");
        email.sendKeys(authorizationEmail);
        password.sendKeys(authorizationPassword);
        submit.click();

        //проверка заголовка новой страницы
        WebElement header = (new WebDriverWait(driver, 20))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='content__centered']/h3")));
        assertEquals("Создание бизнеса", header.getText());

        // добавляем селл поинт
        WebElement addSellPoint = driver.findElement(By.xpath("//button[@class='button button_white form_sellPoints__addButton']"));
        addSellPoint.click();

        WebElement companyName = driver.findElement(By.
                name("companyName"));

        WebElement sellPoint1Name = driver.findElement(By.
                xpath("//div[@class='form_sellPoints__sellPointsWrap']/form[1]/div/div/div[1]/div[1]/div/input[@name='name']"));
        WebElement sellPoint1Address = driver.findElement(By.
                xpath("//div[@class='form_sellPoints__sellPointsWrap']/form[1]/div/div/div[2]/div/div/textarea[@name='address']"));
        WebElement sellPoint1INN = driver.findElement(By.
                xpath("//div[@class='form_sellPoints__sellPointsWrap']/form[1]/div/div/div[2]/div[2]/div/input[@name='inn']"));
        WebElement sellPoint1UseTables = driver.findElement(By.
                xpath("//div[@class='form_sellPoints__sellPointsWrap']/form[1]/div/div/div[3]/div/label/span"));

        WebElement sellPoint2Name = driver.findElement(By.
                xpath("//div[@class='form_sellPoints__sellPointsWrap']/form[2]/div/div/div[1]/div[1]/div/input[@name='name']"));
        WebElement sellPoint2Address = driver.findElement(By.
                xpath("//div[@class='form_sellPoints__sellPointsWrap']/form[2]/div/div/div[2]/div/div/textarea[@name='address']"));
        WebElement sellPoint2INN = driver.findElement(By.
                xpath("//div[@class='form_sellPoints__sellPointsWrap']/form[2]/div/div/div[2]/div[2]/div/input[@name='inn']"));
        WebElement sellPoint2UseTables = driver.findElement(By.
                xpath("//div[@class='form_sellPoints__sellPointsWrap']/form[2]/div/div/div[3]/div/label/span"));
        WebElement save = driver.findElement(By.xpath("//button[@class='button']"));


        companyName.sendKeys("Мой бизнес");
        sellPoint1Name.sendKeys("ТТ1");
        sellPoint1Address.sendKeys("Дворцовая пл., д. 1");
        sellPoint1INN.sendKeys("0011223344");
        sellPoint1UseTables.click();
        sellPoint2Name.sendKeys("ТТ2");
        sellPoint2Address.sendKeys("Красная пл., д. 1");
        sellPoint2INN.sendKeys("9988776655");
        sellPoint2UseTables.click();

        save.click();

        // конфигурация залов
        //проверка заголовка новой страницы

        // stupidWait(1000);
        WebElement panel = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//form[@class='form']")));

        header = driver.findElement(By.xpath("//div[@class='content__centered']/h3"));
        assertEquals("Настройка залов", header.getText());

        //заполнение названий залов

        WebElement sellPoint1Room1 = driver.findElement(By.
                xpath("//div[1][@class='panel']/div/div[2]/div/div/input"));
        sellPoint1Room1.click();
        sellPoint1Room1.sendKeys("Большой зал");

        WebElement sellPoint1Room2 = driver.findElement(By.
                xpath("//div[1][@class='panel']/div/div[3]/div/div/input"));
        sellPoint1Room2.click();
        sellPoint1Room2.sendKeys("Средний зал");

        WebElement sellPoint2Room1 = driver.findElement(By.
                xpath("//div[2][@class='panel']/div/div[2]/div/div[1]/input"));

        sellPoint2Room1.click();
        sellPoint2Room1.sendKeys("Малый зал");

        //убираем фокус на другой контрол
        sellPoint1Room1.click();

        //начинаем работу под новым юзером
        WebElement begin = driver.findElement(By.xpath("//button"));
       begin.submit();

/*
       //дебаг - логин
        WebElement login = driver.findElement(By.name("username"));
        WebElement password = driver.findElement(By.name("password"));
        WebElement submit = driver.findElement(By.xpath("//button"));

        login.sendKeys("duxy7@list.ru");
        password.sendKeys("baranova");
        submit.click();
*/
       // проверка заголовка
        WebElement contextName=driver.findElement(By.xpath("//div[@class='head__title']"));
        assertEquals("Мой бизнес",contextName.getText());

        // проверка данных на форме торговых точек
        checkSellPointData(1,"ТТ1","Дворцовая пл., д. 1","0011223344","Меню 1");
        checkSellPointData(2,"ТТ2","Красная пл., д. 1","9988776655","Меню 1");

        //логаут
        logoutApp();



    }

}
